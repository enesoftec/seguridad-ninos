package com.ene.seguridadn;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PointF;
import android.speech.RecognizerIntent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private View vParent;
    private ImageView btnScan, imageView;

    private float initVerticalSlider1, initVerticalSlider2, posVertical;
    private boolean speaking = false;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    //Commands
    private ArrayList<Command> listCommands;

    //
    private BluetoothAdapter btAdapter;
    private final String MAC_ADDRESS = "20:15:04:17:66:03";
    private boolean isConnected;
    private ProgressDialog progressConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        vParent = findViewById(R.id.vgParentVerticalSlider);
        btnScan = (ImageView) findViewById(R.id.btnScan);
        imageView = (ImageView) findViewById(R.id.ivBackground);

        btnScan.setOnTouchListener(touchDragDropVerticalSlider());
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Empty
            }
        });


        vParent.post(new Runnable() {
            @Override
            public void run() {
                initVerticalSlider1 = btnScan.getY();
                initVerticalSlider2 = imageView.getY();
            }
        });


        //Listado de Comandos
        listCommands = new ArrayList<>();
        listCommands.add(new Command("Conectar", "Este commando connecta la aplicacion con el dispositivo"));
        listCommands.add(new Command("Desconectar", "Este commando desconecta la aplicacion con el dispositivo"));
        listCommands.add(new Command("Bloquear Puerta Izquierda", "Este comando bloquea la puerta izquierda del automovil"));
        listCommands.add(new Command("Desbloquear Puerta Izquierda", "Este comando desbloquea la puerta izquierda del automovil"));
        listCommands.add(new Command("Bloquear Puerta Derecha", "Este comando bloquea la puerta derecha del automovil"));
        listCommands.add(new Command("Desbloquear Puerta Derecha", "Este comando desbloquea la puerta derecha del automovil"));
        listCommands.add(new Command("Bloquear Puertas", "Este comando bloquea la puertas (izquierda y derecha) del automovil"));
        listCommands.add(new Command("Desbloquear Puertas", "Este comando desbloquea la puertas (izquierda y derecha) del automovil"));


        //Get Bluettoth Adapter
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        // Check smartphone support Bluetooth
        if (btAdapter == null) {
            //Device does not support Bluetooth
            finish();
        }

        // Check Bluetooth enabled
        if (!btAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        }

        progressConnect = new ProgressDialog(MainActivity.this);
        progressConnect.setMessage("Conectando...");
        progressConnect.setCancelable(false);

        // Service
        IntentFilter filterService = new IntentFilter(ConnectService.ACTION_RUN_ISERVICE);

        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()){
                    case ConnectService.ACTION_RUN_ISERVICE:

                        progressConnect.dismiss();

                        isConnected = intent.getBooleanExtra(ConnectService.EXTRA_CONNECT, false);
                        boolean offline = intent.getBooleanExtra(ConnectService.EXTRA_OFFLINE, false);
                        boolean lost = intent.getBooleanExtra(ConnectService.EXTRA_LOST_CONNECTION, false);
                        if(isConnected) {
                            setTitle(getString(R.string.app_name) + " - Conectado");
                        }else
                            setTitle(getString(R.string.app_name) + " - Desconectado");

                        if(offline) {
                            showAlert("Verifica que el dispositivo esté encendido e intenta de nuevo");
                        }

                        if(lost && isConnected) {
                            showAlert("La Conexión se ha perdido, intenta conectar de nuevo");
                        }

                        break;
                }
            }
        };
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(receiver, filterService);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_commands:
                showDialog();
                break;
            case R.id.action_about:

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQ_CODE_SPEECH_INPUT && null != data) {
            ArrayList<String> result = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

            processCommand(result.get(0));
            System.out.println("VozTexto: " + result.get(0));
        }
    }

    private void connect(){
        if (!isConnected) {
            Set<BluetoothDevice> pariedDevices = btAdapter.getBondedDevices();
            BluetoothDevice btDevice = null;

            for (BluetoothDevice device : pariedDevices) {
                if (device.getAddress().equals(MAC_ADDRESS)) {
                    btDevice = device;
                    System.out.println("Device Found!");
                    break;
                }
            }

            if (btDevice != null) {
                ConnectService.BluetoothDevice = btDevice;

                Intent connect = new Intent(MainActivity.this, ConnectService.class)
                        .setAction(ConnectService.ACTION_RUN_ISERVICE);
                startService(connect);
                progressConnect.show();
            }else{
                showAlert("No se encuentra el dispositivo");
            }
        }else{
            showAlert("Ya existe una conexion");
        }
    }

    private void disconnect(){
        if(isConnected) {
            Intent connect = new Intent(MainActivity.this, ConnectService.class)
                    .setAction(ConnectService.ACTION_RUN_ISERVICE);
            stopService(connect);
            isConnected = false;
        }else{
            showAlert("No hay conexion disponible para desconectar");
        }
    }



    private void processCommand(String text){
        int pos = -1;
        for(int i=0; i<listCommands.size(); i++) {
            if (listCommands.get(i).getName().toLowerCase().equals(text.toLowerCase())) {
                pos = i;
                break;
            }
        }

        if(pos >= 0){
            String message = "";
            boolean notApply = false;
            switch (pos){
                case 0:
                    connect();
                    System.out.println("Se ha activado la conexion");
                    notApply = true;
                    break;
                case 1:
                    disconnect();
                    System.out.println("Se ha desactivado la conexion");
                    notApply = true;
                    break;
                case 2:
                    ConnectService.sendCommand("LEFTL");
                    message = "Bloqueando Puerta Izquierda ...";
                    break;
                case 3:
                    ConnectService.sendCommand("LEFTU");
                    message = "Desbloqueando Puerta Izquierda ...";
                    break;
                case 4:
                    ConnectService.sendCommand("RIGHTL");
                    message = "Bloqueando Puerta Derecha ...";
                    break;
                case 5:
                    ConnectService.sendCommand("RIGHTU");
                    message = "Desbloqueando Puerta Derecha ...";
                    break;
                case 6:
                    ConnectService.sendCommand("BOTHL");
                    message = "Bloqueando Puertas ...";
                    break;
                case 7:
                    ConnectService.sendCommand("BOTHU");
                    message = "Desbloqueando Puertas ...";
                    break;
            }
            if(isConnected)
                showToastMessage(message);
            else if(!notApply)
                showToastMessage("No hay conexión con el dispositivo");
        }else{
            showAlert("No se reconoce el Comando");
            System.out.println("Comando Irreconocible");
        }
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "No esta soportado",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void showDialog(){

        final Dialog dialog = new Dialog(this);
        Window window = dialog.getWindow();

        window.requestFeature(Window.FEATURE_NO_TITLE);

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        WindowManager.LayoutParams layoutParams = window.getAttributes();

        dialog.getWindow().setAttributes(layoutParams);

        dialog.setContentView(R.layout.dialog_commands);


        final Spinner spCommands = (Spinner) dialog.findViewById(R.id.spCommands);
        final TextView txtDescription = (TextView) dialog.findViewById(R.id.txtDescription);

        spCommands.setAdapter(new ListAdapter(this, R.layout.item_command,
                listCommands) {
            @Override
            public void onEntrada(Object entrada, View view, int position) {
                TextView text = (TextView) view.findViewById(R.id.txt_command);
                String sText = ((Command)entrada).getName().toUpperCase();
                text.setText(sText);
            }
        });

        spCommands.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txtDescription.setText(listCommands.get(position).getDescription());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialog.show();
    }


    private void showAlert(String message) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("Aceptar", null)
                .show();
    }

    private void showToastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private View.OnTouchListener touchDragDropVerticalSlider(){
        final int ratio = 100;

        View.OnTouchListener touch = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                PointF pfStart1 = new PointF(view.getX(), view.getY());
                PointF pfStart2 = new PointF(imageView.getX(), imageView.getY());
                PointF pfDown1 = new PointF(), pfDown2 = new PointF();

                switch (motionEvent.getAction()){

                    case MotionEvent.ACTION_DOWN:
                        pfDown1.y = motionEvent.getY();
                        pfDown2.y = motionEvent.getY();

                        speaking = false;
                        break;

                    case MotionEvent.ACTION_MOVE:

                        PointF moving1 = new PointF(motionEvent.getX() - pfDown1.x, motionEvent.getY() - pfDown1.y);
                        PointF moving2 = new PointF(motionEvent.getX() - pfDown2.x, motionEvent.getY() - pfDown2.y);

                        posVertical = pfStart1.y + moving1.y - ratio;

                        boolean upping = true;

                        float percent = (posVertical * 100) / initVerticalSlider1;

                        if(pfStart1.y < posVertical) {
                            System.out.println("Estas Bajandooo");
                            if(percent > 100)
                                upping = false;
                        }else {
                            System.out.println("Estas Subiendoo");
                            if(!speaking && percent < 70){
                                //Start Action
                                promptSpeechInput();

                                //view.setY(initVerticalSlider1);
                                // imageView.setY(initVerticalSlider2);
                                speaking = true;
                                return false;
                            }
                        }
                        if(upping && !speaking) {
                            view.setY(posVertical);
                            imageView.setY(pfStart2.y + moving2.y - ratio);
                        }
                        break;

                    case MotionEvent.ACTION_UP:
                        view.setY(initVerticalSlider1);
                        imageView.setY(initVerticalSlider2);
                        break;
                }

                vParent.invalidate();
                return false;
            }
        };

        return touch;
    }
}
