package com.ene.seguridadn;

/**
 * Created by ENESOFTEC on 11/10/2016.
 */

public class Command {
    private String name;
    private String description;

    public Command(String n, String d){
        name = n;
        description = d;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }
}
