package com.ene.seguridadn;

import android.app.IntentService;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class ConnectService extends IntentService implements ConnectionBluetooth.BluetoothListener {

    private final String TAG = "ConnectService";

    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_RUN_ISERVICE = "com.ene.seguridadn.action.RUN_INTENT_SERVICE";

    // TODO: Rename parameters
    public static final String EXTRA_CONNECT = "com.ene.seguridadn.extra.CONNECT";
    public static final String EXTRA_OFFLINE = "com.ene.seguridadn.extra.OFFLINE";
    public static final String EXTRA_LOST_CONNECTION = "com.ene.seguridadn.extra.LOST_CONNECTION";

    public static BluetoothDevice BluetoothDevice;

    private static ConnectionBluetooth bluetooth;

    private static boolean connected = true;

    public ConnectService() {
        super("ConnectService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            if (ACTION_RUN_ISERVICE.equals(intent.getAction())) {
                bluetooth = new ConnectionBluetooth(ConnectService.this, BluetoothDevice);
                handleAction();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters
     */
    private void handleAction() {

        if(bluetooth != null){
            bluetooth.ConnectBT();
            int cont = 0;
            while(true){
                if(!connected) {
                    if(cont != 0){
                        lostConnection();
                        break;
                    }
                    while (true) {
                        if (connected) break;
                        bluetooth.ConnectBT();

                        if(cont > 1){
                            offline();
                            break;
                        }

                        Log.d(TAG, "Segundo: " + cont);

                        cont++;
                        waitSeconds(3);
                    }

                    if(!connected)
                        break;
                }else
                    cont = -1;

                waitSeconds(1);
            }
        }

    }

    private void waitSeconds(int seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sendCommand(String command){
        if(bluetooth != null)
            bluetooth.sendData(command);
    }

    @Override
    public void onDestroy() {
        bluetooth.CloseBT();
        Intent intent = new Intent(ACTION_RUN_ISERVICE)
                .putExtra(EXTRA_CONNECT, false);
        LocalBroadcastManager.getInstance(ConnectService.this).sendBroadcast(intent);
    }


    @Override
    public void onSuccess() {
        connected = true;

        Intent intent = new Intent(ACTION_RUN_ISERVICE)
                .putExtra(EXTRA_CONNECT, connected);
        LocalBroadcastManager.getInstance(ConnectService.this).sendBroadcast(intent);
    }

    private void offline(){
        Intent intent = new Intent(ACTION_RUN_ISERVICE)
                .putExtra(EXTRA_OFFLINE, true);
        LocalBroadcastManager.getInstance(ConnectService.this).sendBroadcast(intent);
    }

    private void lostConnection(){
        Intent intent = new Intent(ACTION_RUN_ISERVICE)
                .putExtra(EXTRA_LOST_CONNECTION, true);
        LocalBroadcastManager.getInstance(ConnectService.this).sendBroadcast(intent);
    }

    @Override
    public void onError() {
        connected = false;
        Intent intent = new Intent(ACTION_RUN_ISERVICE)
                .putExtra(EXTRA_CONNECT, connected);
        LocalBroadcastManager.getInstance(ConnectService.this).sendBroadcast(intent);
    }

    @Override
    public void onDataReceived(String data) {

    }
}
