package com.ene.seguridadn;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

/**
 * Created by ENE on 29/01/2016.
 */
public class ConnectionBluetooth {

    //private static final UUID myUUID = UUID.fromString("E0CBF06C-CD8B-4647-BB8A-263B43F0F974");
    private static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //Handler Bluetooth
    private Handler btHandler;
    private final int handlerState = 0; //Id handler

    private StringBuilder recDataString = new StringBuilder();

    private ConnectedThread mConnectedThread;

    private BluetoothSocket btSocket = null;
    private BluetoothDevice btDevice = null;

    private boolean CONNECTED = false;

    private BluetoothListener listener;

    public ConnectionBluetooth(ConnectService parent, BluetoothDevice btd){

        btDevice = btd;

        listener = parent;

        btHandler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {                                     //if message is what we want
                    String readMessage = (String) msg.obj;                                                                // msg.arg1 = bytes from connect thread
                    recDataString.append(readMessage);                                      //keep appending to string until ~
                    int endOfLineIndex = recDataString.indexOf("~");                    // determine the end-of-line
                    if (endOfLineIndex > 0) {                                          // make sure there data before ~
                        String dataInPrint = recDataString.substring(0, endOfLineIndex);    // extract string
                        Log.i("Data Received", dataInPrint);

                        if (recDataString.charAt(0) == '#')                             //if it starts with # we know it is what we are looking for
                        {
                            listener.onDataReceived(recDataString.substring(1, endOfLineIndex));
                        }
                        recDataString.delete(0, recDataString.length());                    //clear all string data
                    }
                }
            }
        };

    }


    private BluetoothSocket createBluetoothSocket(BluetoothDevice device, boolean first) throws IOException {
        BluetoothSocket bts;
        if(first)
            bts = device.createInsecureRfcommSocketToServiceRecord(myUUID);
        else
            try {
                bts = (BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[]{int.class}).invoke(btDevice, 1);
            } catch (Exception e) {
                e.printStackTrace();
                bts = null;
            }
        //creates secure outgoing connecetion with BT device using UUID
        return bts;
    }

    public void ConnectBT(){

        boolean connected = false;
        try {
            btSocket = createBluetoothSocket(btDevice, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Establish the Bluetooth socket connection.
        try
        {
            btSocket.connect();
            connected = true;
        } catch (IOException e) {
            e.printStackTrace();
            try
            {
                btSocket = createBluetoothSocket(btDevice, false);
                btSocket.connect();
                connected = true;
            } catch (IOException e2){
                e2.printStackTrace();
                try {
                    btSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }

        if(connected) {
            mConnectedThread = new ConnectedThread(btSocket);
            mConnectedThread.start();
        }

        CONNECTED = connected;
        if(connected) {
            listener.onSuccess();
            System.out.println("BT Connected!!");
        }else {
            listener.onError();
            System.out.println("BT Not Connected!!");
        }
    }

    public void CloseBT(){
        if(btSocket != null){
            try {
                CONNECTED = false;
                btSocket.close();
                Log.i("CloseBT", "Conexión Cerrada");
            } catch (IOException e) {
                Log.i("CloseBT", "Error al Cerrar");
                e.printStackTrace();

            }
        }
    }

    public void sendData(String data){
        mConnectedThread.write(data);
    }


    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;
            // Keep looping to listen for received messages
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);

                    // Send the obtained bytes to the UI Activity via handler
                    btHandler.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    e.printStackTrace();
                    listener.onError();
                    break;
                }
            }
        }
        //write method
        public void write(String command) {
            System.out.println("Enviando comando " + command);
            String input = command + "\n";

            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                listener.onError();
                //finish();

            }
        }
    }

    public String AddressDeviceConnected(){
        if(CONNECTED)
            return btDevice.getAddress();
        else
            return null;
    }

    public interface BluetoothListener{
        void onSuccess();
        void onError();
        void onDataReceived(String data);
    }
}
